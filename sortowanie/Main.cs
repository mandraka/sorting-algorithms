﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sortowanie
{
    public partial class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Wprowadz ilość elementów w tablicy");
            int n = 0;
            int.TryParse(Console.ReadLine(), out n);
            int[] tab = new int[n];
            int y = 0;
            for (int i = 0; i < n; i++)
            {
                y = i + 1;
                Console.WriteLine("Wprowadź wartość " + y + " elementu");
                int.TryParse(Console.ReadLine(), out tab[i]); ;
            }
            ConsoleKeyInfo k2;
            int c2 = 0;
            Console.WriteLine("Czy chcesz posortować:\nwybieraniem (1) \nrozstawianiem (2)?\nmerge sort (3)\nquicksort (4)");
            k2 = Console.ReadKey();
            if (k2.KeyChar == '1')
                c2 = 1;
            else if (k2.KeyChar == '2')
                c2 = 2;
            else if (k2.KeyChar == '3')
                c2 = 3;
            else if (k2.KeyChar == '4')
                c2 = 4;
            Console.WriteLine();

            bool descendFlag = false;
            Console.WriteLine("Czy chcesz sortować rosnąco (0) czy malejąco (1)?");
            k2 = Console.ReadKey();
            if (k2.KeyChar == '1')
                descendFlag = true;
            Console.WriteLine();
            Console.WriteLine();

            long elapsedMs = 0;
            var watch = System.Diagnostics.Stopwatch.StartNew();
            switch (c2)
            {
                case 1: //wybieranie (zamiana)
                    int c = 0;
                    for (int i = 0; i < n; i++)
                    {
                        c = tab[i];
                        for (int j = i + 1; j < n; j++)
                        {
                            if (tab[j] < tab[i])
                            {
                                c = tab[i];
                                tab[i] = tab[j];
                                tab[j] = c;
                            }
                        }
                    }
                    if (descendFlag == true)
                        Array.Reverse(tab);
                    foreach (int d in tab)
                        Console.WriteLine(d);
                    watch.Stop();
                    elapsedMs = watch.ElapsedMilliseconds;
                    Console.WriteLine();
                    Console.WriteLine("Wykonanie sortowania zajęło " + elapsedMs + " ms");
                    break;
                case 2: //rozstawianie (przesuwający sie przedział)
                    int indeks = 0, a = 0, b = 0;
                    for (int i = 1; i < n; i++)
                    {
                        if (tab[i] <= tab[indeks])
                        {
                            a = indeks;
                            while (a > 0)
                            {
                                if (tab[a - 1] < tab[i])
                                    break;
                                a--;
                            }
                            b = tab[i];
                            for (int j = i; j > a; j--)
                            {
                                tab[j] = tab[j - 1];
                            }
                            tab[a] = b;
                            indeks++;
                        }
                        else
                        {
                            a = indeks + 1;
                            while (a < n - 1)
                            {
                                if (tab[a] >= tab[i])
                                    break;
                                a++;
                            }
                            b = tab[i];
                            for (int j = i; j > a; j--)
                            {
                                tab[j] = tab[j - 1];
                            }
                            tab[a] = b;
                        }
                    }
                    if (descendFlag == true)
                        Array.Reverse(tab);
                    foreach (int d in tab)
                        Console.WriteLine(d);
                    watch.Stop();
                    elapsedMs = watch.ElapsedMilliseconds;
                    Console.WriteLine();
                    Console.WriteLine("Wykonanie sortowania zajęło " + elapsedMs + " ms");
                    break;
                case 3: //merge sort
                    tab =mergesort(tab);
                    if (descendFlag == true)
                        Array.Reverse(tab);
                    foreach (int d in tab)
                        Console.WriteLine(d);
                    watch.Stop();
                    elapsedMs = watch.ElapsedMilliseconds;
                    Console.WriteLine();
                    Console.WriteLine("Wykonanie sortowania zajęło "
                        + elapsedMs + " ms");
                    break;
                case 4: //quicksort
                    tab = quicksort(tab);
                    if (descendFlag == true)
                        Array.Reverse(tab);
                    foreach (int d in tab)
                        Console.WriteLine(d);
                    watch.Stop();
                    elapsedMs = watch.ElapsedMilliseconds;
                    Console.WriteLine();
                    Console.WriteLine("Wykonanie sortowania zajęło " + elapsedMs + " ms");
                    break;
            }
            Console.ReadKey();
        }
        
    }
}
