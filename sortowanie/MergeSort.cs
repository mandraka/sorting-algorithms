﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sortowanie
{
    public partial class Program
    {
        static int[] mergesort(int[] tab)
        {
            //dzielenie
            if (tab.Length > 1)
            {
                int range;
                int[] leftTab, rightTab;
                // dla parzystej 
                if (tab.Length % 2 == 0)
                {
                    range = tab.Length / 2;
                    leftTab = new int[range];
                    rightTab = new int[range];
                    for (int i = 0; i < range; i++)
                    {
                        leftTab[i] = tab[i];
                        rightTab[i] = tab[i + range];
                    }
                    leftTab= mergesort(leftTab);
                    rightTab = mergesort(rightTab);
                    //scalenie
                    int leftIndex = 0; int rightIndex = 0;
                    for (int i = 0; i < tab.Length; i++)
                    {
                        if (leftIndex < range && rightIndex < range)
                        {
                            if (leftTab[leftIndex] < rightTab[rightIndex])
                            {
                                tab[i] = leftTab[leftIndex];
                                leftIndex++;
                            }
                            else
                            {
                                tab[i] = rightTab[rightIndex];
                                rightIndex++;
                            }
                        }
                        else if (leftIndex < range)
                        {
                            tab[i] = leftTab[leftIndex];
                            leftIndex++;
                        }
                        else if (rightIndex < range)
                        {
                            tab[i] = rightTab[rightIndex];
                            rightIndex++;
                        }
                    }
                }
                //dla nieparzystej
                else
                {
                    range = tab.Length / 2;
                    leftTab = new int[range + 1];
                    rightTab = new int[range];
                    for (int i = 0; i < range; i++)
                    {
                        leftTab[i] = tab[i];
                        rightTab[i] = tab[i + range + 1];
                    }
                    leftTab[range] = tab[range];
                    leftTab= mergesort(leftTab);
                    rightTab=mergesort(rightTab);
                    //scalenie
                    int leftIndex = 0; int rightIndex = 0;
                    for (int i = 0; i < tab.Length; i++)
                    {
                        if (leftIndex < range+1 && rightIndex < range)
                        {
                            if (leftTab[leftIndex] < rightTab[rightIndex])
                            {
                                tab[i] = leftTab[leftIndex];
                                leftIndex++;
                            }
                            else
                            {
                                tab[i] = rightTab[rightIndex];
                                rightIndex++;
                            }
                        }
                        else if (leftIndex < range+1)
                        {
                            tab[i] = leftTab[leftIndex];
                            leftIndex++;
                        }
                        else if (rightIndex < range)
                        {
                            tab[i] = rightTab[rightIndex];
                            rightIndex++;
                        }
                    }
                }
            }
            //rządzenie

            return tab;
        }
    }
}
