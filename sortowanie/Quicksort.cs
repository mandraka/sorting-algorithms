﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sortowanie
{
    public partial class Program
    {
        static int[] quicksort(int[] tab)
        {
            if (tab.Length > 1)
            {
                int a = tab[0];
                int countOfElements = tab.Length;
                List<int> largerArray = new List<int>();
                List<int> smallerArray = new List<int>();
                for (int i = 1; i < countOfElements; i++)
                {
                    if (tab[i] > a)
                        largerArray.Add(tab[i]);
                    else if (tab[i] <= a)
                        smallerArray.Add(tab[i]);
                }
                List<int> finalArray = new List<int>();
                finalArray.AddRange(quicksort(smallerArray.ToArray()));
                finalArray.Add(a);
                finalArray.AddRange(quicksort(largerArray.ToArray()));
                return finalArray.ToArray();
            }
            else
                return tab;
        }
    }
}
